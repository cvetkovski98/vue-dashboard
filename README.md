# vue-dashboard

[![Netlify Status](https://api.netlify.com/api/v1/badges/52695576-eaa2-406d-96c1-26c6e3554a34/deploy-status)](https://app.netlify.com/sites/nvddashboard/deploys)

## Advanced Web Design Course Project

| Name & Lastname   | Index  |
| ----------------- | ------ |
| Andrej Anchevski  | 175006 |
| Oliver Cvetkovski | 171083 |