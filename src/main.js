import {createApp} from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import './index.css'
import VueApexCharts from "vue3-apexcharts";

createApp(App)
    .use(router)
    .use(VueApexCharts)
    .mount('#app')
