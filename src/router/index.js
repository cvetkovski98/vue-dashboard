import {createRouter, createWebHistory} from 'vue-router'
import Dashboard from '../views/Dashboard.vue'

export const routes = [
    {
        path: '/',
        name: 'Dashboard',
        meta: {
            isTopLevelRoute: true
        },
        component: Dashboard
    },
    {
        path: '/analytics',
        name: 'Analytics',
        meta: {
            isTopLevelRoute: true
        },
        component: () => import('../views/Analytics')
    },
    {
        path: '/sites',
        name: 'Sites',
        meta: {
            isTopLevelRoute: true
        },
        component: () => import('../views/Sites'),
    },
    {
        path: '/site/:id',
        name: 'Site Details',
        meta: {
            isTopLevelRoute: false
        },
        component: () => import('../views/SiteDetails')
    },
    {
        path: '/users',
        name: 'Users',
        meta: {
            isTopLevelRoute: true
        },
        component: () => import('../views/Users')
    }
]

export const path_icon_mapper = {
    '/': '<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>',
    '/analytics': '<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z"></path></svg>',
    '/sites': '<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg>',
    '/users': '<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>'
}

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
