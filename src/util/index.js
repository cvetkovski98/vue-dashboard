export function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const month_name_mapper = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}

export const getRandomChartConfig = (min, max, type, title = '', seriesTypes = ['line', 'line'], curveType = 'smooth',) => {
    let config = {
        series: [],
        categories: [],
        type: type,
        title: title,
        curveType: curveType
    }

    const series1 = {
        name: "Number of page visits",
        data: [],
        type: seriesTypes[0]
    }
    const series2 = {
        name: "Amount of revenue gained",
        data: [],
        type: seriesTypes[1]
    }

    for (let year = 2019; year <= 2021; year++) {
        for (let month = 1; month <= 12; month++) {
            config.categories.push(`${month_name_mapper[month]}.${year}`)
            series1.data.push(getRndInteger(min, max))
            series2.data.push(getRndInteger(min * 0.4, max * 1.4))
        }
    }


    config.series.push(series1, series2)

    return config
}

export const generateSeriesDataAndCategory = (min, max, year1 = 2018, year2 = 2020) => {
    const series = []
    const category = []
    for (let year = year1; year <= year2; year++) {
        for (let month = 1; month <= 12; month++) {
            category.push(`${month_name_mapper[month]}.${year}`)
            series.push(getRndInteger(min, max))
        }
    }
    return {
        series,
        category
    }
}