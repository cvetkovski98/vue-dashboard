module.exports = {
    purge: {
        content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
        options: {
            safelist: [
                'xl:grid-cols-1',
                'xl:grid-cols-2',
                'xl:grid-cols-3',
                'lg:grid-col-1',
                'lg:grid-col-2',
                'lg:grid-col-3',
                'md:grid-col-1',
                'md:grid-col-2',
                'md:grid-col-3'
            ]
        }
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {
            cursor: ['hover', 'focus']
        },
    },
    plugins: [],
}
